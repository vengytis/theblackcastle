﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection.Emit;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerCollisions : MonoBehaviour
{

    public AudioClip clip;
    private bool Chest_Opened = false;
    private bool Key_Taken = false;
    private bool HasCollidedWithSpikes = false;
	private bool HasCollidedWithBall = false;
    private FirebaseConn fb;
    private PlayerDataPost playerData;

    private void Start()
    {
        fb = new FirebaseConn();
    }

    private void Update()
    {
        SetBackTriggers();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Spikes")
        {
            if (!HasCollidedWithSpikes)
            {
                HasCollidedWithSpikes = true;
                HealthMonitor.LoseHP();
            }
        }

        if (collision.gameObject.tag == "Collectable")
        {
            AudioSource audio = GetComponent<AudioSource>();

            if (collision.gameObject.name == "key")
            {
                clip = Resources.Load<AudioClip>("Sounds/key_sound");
                Key_Taken = true;
            }

            else if (collision.gameObject.name == "HP")
            {
                HealthMonitor.AddHP();
                clip = Resources.Load<AudioClip>("Sounds/Get_HP");
            }

            else
            {
                SceneMonitor.AddScore(1);
                clip = Resources.Load<AudioClip>("Sounds/Collect_Diamond");
            }

            audio.clip = clip;
            audio.Play();
            Destroy(collision.gameObject);
        }

        if (collision.gameObject.tag == "Chest")
        {
            if (!Chest_Opened && Key_Taken)
            {
                Light light = collision.GetComponent<Light>();
                SpriteRenderer rend = collision.GetComponent<SpriteRenderer>();
                Sprite[] sprites = Resources.LoadAll<Sprite>("Chest");
                rend.sprite = sprites[1];
                light.enabled = true;
                AudioSource audio = collision.GetComponent<AudioSource>();
                audio.Play();

                SceneMonitor.AddScore(20);
                Chest_Opened = true;
            }
        }

        if (collision.gameObject.tag == "Finish")
        {
			if (SceneManager.GetActiveScene ().name == "Cellar")
				SceneManager.LoadScene (3);
			else {
				string sceneName = SceneManager.GetActiveScene ().name.Substring (0, 6);
				int level = int.Parse (SceneManager.GetActiveScene ().name.Substring (6));
                /* playerData = new PlayerDataPost(playerData.uid, email.text, 0, 0);
                 string jsonData = JsonUtility.ToJson(playerData);
                 */
			 //   fb.UpdateUserData();
				SceneManager.LoadScene (sceneName + (level + 1).ToString ());
			}

        }

		if (collision.gameObject.tag == "Cellar")
		{
			SceneManager.LoadScene("Cellar");
		}

		if (collision.gameObject.tag == "Trigger")
		{
			TrapEventSystem.Set_Level_2_Trigger ();
		}
    }

    void SetBackTriggers()
    {
        HasCollidedWithSpikes = false;
    }

	private void OnCollisionEnter2D(Collision2D collision)
	{
		if (collision.gameObject.tag == "Ball")
		{
			if (!HasCollidedWithBall)
			{
				HasCollidedWithBall = true;
				HealthMonitor.LoseHP();
			}
		}
	}
}
