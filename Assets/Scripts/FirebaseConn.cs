﻿using System.Collections;
using System.Text;
using Firebase.Auth;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class FirebaseConn : MonoBehaviour
{
    private const string USERS_URL = "http://localhost:3000/players/";

    [SerializeField]
    private Text email;

    [SerializeField]
    private Text password;

    [SerializeField]
    private Text warningText;

    internal PlayerDataGet player;
    internal PlayerDataPost playerData;
    internal Firebase.Auth.FirebaseUser newUser;

    private Firebase.Auth.FirebaseAuth auth;

   // private SceneMonitor SceneMonitor;

    private void Start()
    {
        auth = Firebase.Auth.FirebaseAuth.DefaultInstance;
    }

    private IEnumerator GetUserData(string _url, string _uid)
    {
        using (UnityWebRequest www = UnityWebRequest.Get(_url + _uid))
        {
            yield return www.SendWebRequest();
            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
                Debug.Log("Response: " + www.responseCode);
            }
            else
            {
                if (www.isDone)
                {
                    Debug.Log("Response: " + www.responseCode);
                    string jsonResult = Encoding.UTF8.GetString(www.downloadHandler.data);
                    Debug.Log(jsonResult);
                    player = JsonUtility.FromJson<PlayerDataGet>(jsonResult);
                    playerData = new PlayerDataPost(player.uid, player.email, player.points, player.timePerRun);
                    SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
                    // menuAnimator.SetTrigger("OutOfScene");
                    //sceneManagement.LoadSceneWithDelay(2);
                }
            }
        }
    }

    IEnumerator SendData(string _url, string _uid, string _json, string _method)
    {
        Debug.Log("Got some results!");
        var _request = new UnityWebRequest(_url, _method);
        byte[] _playerData = new UTF8Encoding().GetBytes(_json);
        _request.uploadHandler = new UploadHandlerRaw(_playerData);
        _request.downloadHandler = new DownloadHandlerBuffer();
        _request.SetRequestHeader("Content-Type", "application/json");

        yield return _request.SendWebRequest();

        if (_request.isNetworkError)
        {
            Debug.Log("Something went wrong, and returned error: " + _request.error);
        }
        else
        {
            Debug.Log("Response: " + _request.downloadHandler.text);
            if (_request.responseCode == 200)
            {
                Debug.Log("Request finished successfully! New User created successfully.");
              //  menuAnimator.SetTrigger("OutOfScene");
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
            }
        }
    }

    public void CreateAccount()
    {
        if (email.text == string.Empty)
        {
            warningText.text = "Enter your email!";
            //warningAnimator.SetTrigger("MoveToScene");
            return;
        }
        else if (password.text == string.Empty)
        {
            warningText.text = "Enter your password!";
            //warningAnimator.SetTrigger("MoveToScene");
            return;
        }
        if (email.text != string.Empty && password.text != string.Empty)
        {
            auth.CreateUserWithEmailAndPasswordAsync(email.text, password.text).ContinueWith(task =>
            {
                if (task.IsCanceled)
                {
                    Debug.LogError("User creation was canceled!");
                    return;
                }
                if (task.IsFaulted)
                {
                    Debug.LogError("User creation encountered an error: " + task.Exception);
                    warningText.text = "Password or email is incorrect!";
                    //warningAnimator.SetTrigger("MoveToScene");
                    return;
                }
                Firebase.Auth.FirebaseUser _newUser = task.Result;
                Debug.LogFormat("User created successfully: User uid: ({0})", _newUser.UserId);
                playerData = new PlayerDataPost(_newUser.UserId, email.text, 0, 0);
                string jsonData = JsonUtility.ToJson(playerData);
                Debug.Log(jsonData);
                //DoOnMainThread.ExecuteOnMainThread.Enqueue(() => { StartCoroutine(SendData(usersURL, "", jsonData, "POST")); });
                StartCoroutine(SendData(USERS_URL, "", jsonData, "POST"));
            });
        }
    }

    public void Login()
    {
        if (email.text == string.Empty)
        {
            warningText.text = "Enter your email!";
            //warningAnimator.SetTrigger("MoveToScene");
            return;
        }
        else if (password.text == string.Empty)
        {
            warningText.text = "Enter your password!";
            //warningAnimator.SetTrigger("MoveToScene");
            return;
        }
        auth.SignInWithEmailAndPasswordAsync(email.text, password.text).ContinueWith(task => {
            if (task.IsCanceled)
            {
                Debug.LogError("User login was canceled.");
                return;
            }
            if (task.IsFaulted)
            {
                Debug.LogError("User login encountered an error: " + task.Exception);
                warningText.text = "Incorrect email or password!";
                //warningAnimator.SetTrigger("MoveToScene");
                return;
            }
            newUser = task.Result;
            Debug.LogFormat("User signed in successfully: User uid: ({0})", newUser.UserId);
            //DoOnMainThread.ExecuteOnMainThread.Enqueue(() => { StartCoroutine(GetUserData(usersURL, newUser.UserId)); });
            StartCoroutine(GetUserData(USERS_URL, newUser.UserId));
        });
    }

    public void UpdateUserData()
    {
        playerData = new PlayerDataPost(newUser.UserId, email.text, SceneMonitor.GetScore(), SceneMonitor.GetTimeElapsed());
        string jsonData = JsonUtility.ToJson(playerData);
        Debug.Log(jsonData);
        //DoOnMainThread.ExecuteOnMainThread.Enqueue(() => { StartCoroutine(SendData(usersURL, player.uid, _newJson, "PUT")); });
        StartCoroutine(SendData(USERS_URL, player.uid, jsonData, "PUT"));
    }

    
}

public class PlayerDataPost
{
    public string uid;
    public string email;
    public int points;
    public int timePerRun;

    public PlayerDataPost(string uid, string email, int points, int timePerRun)
    {
        this.uid = uid;
        this.email = email;
        this.points = points;
        this.timePerRun = timePerRun;
    }
}

public class PlayerDataGet
{
    public string uid;
    public string email;
    public int points;
    public int timePerRun;

    public PlayerDataGet(string uid, string email, int points, int timePerRun)
    {
        this.uid = uid;
        this.email = email;
        this.points = points;
        this.timePerRun = timePerRun;
    }

    public string getCurrentUid()
    {
        return this.uid;
    }

    public void SetUID(string uid)
    {
        this.uid = uid;
    }
}
