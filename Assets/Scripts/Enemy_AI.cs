﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_AI : MonoBehaviour
{

    private float Range = 5f;
    private float Speed = 2f;
    private float AttackRange = 0.7f;
    public GameObject Target;
    public bool isPatrol;
    public static float maxHp = 10;
    public static float currentHp;
    public float HP = 10;

    public float MinX;
    public float MaxX;
    public float PatrolSpeed;


    public Rigidbody2D rb;
    private float Scale;
    private Animator anim;
    public static bool isChasing;

    void Start()
    {
        currentHp = maxHp;
        Target = GameObject.FindGameObjectWithTag("Player");
        rb = GetComponent<Rigidbody2D>();
        Scale = transform.localScale.x;
        anim = GetComponent<Animator>();
        isChasing = false;
        TurnOffColliderForPatrol();
        ChangePatrolSprites();
    }

    void Update()
    {
        if (!isPatrol)
        {
            PlayerInRange();
            if (HP <= 0)
                Destroy(this.gameObject);
        }
        else
            Patrol();

    }

    void PlayerInRange()
    {
        rb.bodyType = RigidbodyType2D.Dynamic;
        float currentRange = Vector2.Distance(transform.position, Target.transform.position);
        if (currentRange <= Range)
        {
            if (currentRange <= AttackRange)
            {
                Attack();
            }

            else if (transform.position.x >= Target.transform.position.x - 0.1 && transform.position.x <= Target.transform.position.x + 0.1)
            {
                if (transform.position.x > Target.transform.position.x)
                    transform.localScale = new Vector2(Scale, Scale);
                else
                    transform.localScale = new Vector2(-Scale, Scale);

                rb.velocity = new Vector2(0, rb.velocity.y);
            }

            else if (transform.position.x > Target.transform.position.x)
            {
                rb.velocity = new Vector2(-Speed, rb.velocity.y);
                transform.localScale = new Vector2(Scale, Scale);
                anim.ResetTrigger("attack");
            }

            else
            {
                rb.velocity = new Vector2(Speed, rb.velocity.y);
                transform.localScale = new Vector2(-Scale, Scale);
                anim.ResetTrigger("attack");
            }

            isChasing = true;
        }
        else
        {
            rb.velocity = new Vector2(0, rb.velocity.y);
            isChasing = false;
        }
    }

    void Attack()
    {
        rb.velocity = new Vector2(0, rb.velocity.y);
        anim.SetTrigger("attack");
    }

    void DealDamage()
    {
        float currentRange = Vector2.Distance(transform.position, Target.transform.position);
        if (currentRange <= AttackRange * 1.2f)
            HealthMonitor.LoseHP();
    }

    void Patrol()
    {
        rb.bodyType = RigidbodyType2D.Kinematic;
        float currentX = Mathf.PingPong(PatrolSpeed * Time.time, MaxX - MinX);
        transform.position = new Vector2(MinX + currentX, transform.position.y);

        if (transform.position.x <= MinX + MinX * 0.005)
        {
            transform.localScale = new Vector2(-Scale, Scale);
        }

        else if (transform.position.x >= MaxX - MinX * 0.005)
        {
            transform.localScale = new Vector2(Scale, Scale);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (isPatrol)
        {
            if (collision.gameObject.tag == "Player")
            {
                HealthMonitor.LoseHP();
            }
        }
    }

    void TurnOffColliderForPatrol()
    {
        if (isPatrol)
            GetComponent<EdgeCollider2D>().enabled = false;
    }

    void ChangePatrolSprites()
    {
        if (!isPatrol)
            GetComponent<ChangeSprites>().enabled = false;
    }

    public void LoseHp(float damage)
    {
        HP -= damage;
        currentHp -= damage;
    }
  /*  public static void ReceiveDamage(float enemyPosX, float projectilePosX)
    {
        if (enemyPosX > projectilePosX)
            rb.AddForce(new Vector2(rb.position.x + 2, rb.position.y + 1));

    }*/
}
