﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : MonoBehaviour {

    public Transform Target;
	public GameObject player;
    private CapsuleCollider2D col;
	private Vector2 offset;
    public bool isMoving;
    public float MinX;
    public float MaxX;
    public float Speed;

    void Start () {
		player = null;
        Target = GameObject.FindGameObjectWithTag("Player").transform;
        col = GetComponent<CapsuleCollider2D>();
	}
	
	void Update () {
        Patrol();
        ColliderLogic();
		MoveWithPlatform ();
	}

    void Patrol()
    {
        if (isMoving)
        {
            float currentX = Mathf.PingPong(Speed * Time.time, MaxX - MinX);
            transform.position = new Vector2(MinX + currentX, transform.position.y);
        }
    }

    void ColliderLogic()
    {
        if (Target.position.y - Target.localScale.y / 3 < transform.position.y)
            col.enabled = false;
        else
            col.enabled = true;
    }

	void OnCollisionStay2D(Collision2D col)
	{
		player = col.gameObject;
		offset = player.transform.position - transform.position;
	}

	void OnCollisionExit2D()
	{
		player = null;
	}

	void MoveWithPlatform()
	{
		if (isMoving)
		if (player != null) {
			player.transform.position = new Vector2(transform.position.x + offset.x, transform.position.y + offset.y);
		}
	}
}
