﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HealthMonitor : MonoBehaviour {

    private static int MaxHP = 4;
    private static int CurrentHP;
    private int StartHp = 3;

    private static AudioClip clip;

    private static float StartingX;
    private static float StartingY;
    private static Transform Player;
    private bool ScoreSet;

    public GameObject GameOverPanel;

    private GameObject[] HP_array = new GameObject[5];

	void Start () {
        ScoreSet = false;
        CurrentHP = StartHp;
        GetChildren();
        GameOverPanel.SetActive(false);
        Player = GameObject.FindGameObjectWithTag("Player").transform;
        StartingX = Player.position.x;
        StartingY = Player.position.y;
    }
	
	void Update () {
        Manager();

        if (CurrentHP <= 0)
        {
            GameOverPanel.SetActive(true);
            if (!ScoreSet)
            {
                HighScore.SetHighScore(SceneMonitor.GetScore());
                ScoreSet = true;
            }
        }

	}

    void GetChildren()
    {
        for (int i = 0; i < transform.childCount; i++)
            HP_array[i] = transform.GetChild(i).gameObject;
    }

    void Manager()
    {
        for (int i = 0; i < CurrentHP; i++)
            HP_array[i].SetActive(true);

        for (int i = CurrentHP; i < MaxHP; i++)
            HP_array[i].SetActive(false);
    }

    public static void LoseHP()
    {
		if (SceneManager.GetActiveScene ().name == "Cellar") {
			CurrentHP -= 1;
			SceneManager.LoadScene ("Cellar");
		}
		else {
			CurrentHP -= 1;
			RelocatePlayer ();
		}
          //  clip = Resources.Load<AudioClip>("Sounds/Lose_HP");
    }

    public static void AddHP()
    {
        if (CurrentHP < MaxHP)
        CurrentHP += 1;
    }

    private static void RelocatePlayer()
    {
        Player.position = new Vector2(StartingX, StartingY);
    }
}
