﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingWall : MonoBehaviour {

	public float Speed;
	public Transform Player;
	private bool Moving;

	void Start () {
		Player = GameObject.FindGameObjectWithTag ("Player").transform;
		Moving = false;
	}

	void Update () {

		if (Player.transform.position.x >= 3)
			Moving = true;

		if (Moving)
		transform.position = new Vector2(transform.position.x + Speed * Time.deltaTime, transform.position.y);
	}
}
