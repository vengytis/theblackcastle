﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class CameraController : MonoBehaviour
{

    public GameObject objectToFollow;

    public float speed = 2.0f;
	private float cameraSize;
    private Vector3 newPos;

    private void Start()
    {
        Vector3 newPos = this.transform.position;
		cameraSize = gameObject.GetComponent<Camera> ().orthographicSize;
		objectToFollow = GameObject.FindGameObjectWithTag("Player");
    }

    void Update()
    {
		//StandartCameraWithFollow ();
		StandartCameraWithFollow();
    }

	void StandartCameraWithFollow()
	{
		float interpolation = speed * Time.deltaTime;

		Vector3 position = this.transform.position;
		position.y = Mathf.Lerp(this.transform.position.y, objectToFollow.transform.position.y + cameraSize/2, interpolation);
		position.x = Mathf.Lerp(this.transform.position.x, objectToFollow.transform.position.x, interpolation);

		if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
		{
			if (position.y <= 2.5f)
				position.y = Mathf.Clamp(Mathf.Lerp(this.transform.position.y, transform.position.y - 1f, interpolation), 2f, newPos.y);
			else
				position.y = Mathf.Clamp(Mathf.Lerp(this.transform.position.y, transform.position.y - 1f, interpolation), newPos.y - 4, newPos.y);

		}
		else
		{
			newPos = this.transform.position;
		}

		this.transform.position = position;
	}
		
}