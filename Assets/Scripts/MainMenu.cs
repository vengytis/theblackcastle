﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour {

    public GameObject ManualGo;
    public GameObject OptionsGo;
    public GameObject HighScoreGo;

    GameObject MainMenuGo;
    Slider VolumeSlider;

    public void Start()
    {
        MainMenuGo = GameObject.FindGameObjectWithTag("MainMenu");
        CloseOthers(MainMenuGo);
    }

    public void Play()
    {
        SceneManager.LoadScene(1);
        SceneMonitor.SetScore(0);
        Time.timeScale = 1;
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void Manual()
    {
        CloseOthers(ManualGo);
        ManualGo.SetActive(true);
    }

    public void Options()
    {
        CloseOthers(OptionsGo);
        OptionsGo.SetActive(true);
        GameObject slider = OptionsGo.transform.Find("Slider").gameObject;
        VolumeSlider = slider.GetComponent<Slider>();
    }

    public void HighScore()
    {
        CloseOthers(HighScoreGo);
        HighScoreGo.SetActive(true);
    }

    public void Back()
    {
        MainMenuGo.SetActive(true);
        CloseOthers(MainMenuGo);
    }

    public void BackToMainMenu()
    {
        SceneManager.LoadScene(0);
    }

    void CloseOthers(GameObject currentGo)
    {
        Transform parent = currentGo.transform.parent;
        foreach (Transform go in parent)
        {
            if (go.gameObject != currentGo.gameObject && go.gameObject.name.Contains("Panel"))
                go.gameObject.SetActive(false);
        }
    }

    public void ChangeSoundVolume()
    {
        PlayerPrefs.SetFloat("Volume", VolumeSlider.value);
    }
}
