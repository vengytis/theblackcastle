﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeSetter : MonoBehaviour
{
    private Text TimeElapsed;

    void Start()
    {
        TimeElapsed = GetComponent<Text>();
        TimeElapsed.text = "Time: 0";
    }

    void Update()
    {
        TimeElapsed.text = "Time: " + SceneMonitor.GetTimeElapsed();
    }
}
