﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeSprites : MonoBehaviour {

	public string SpriteSheetName;
	private string LoadedSpriteSheetName;
	private Dictionary<string, Sprite> spriteSheet;
	private SpriteRenderer spriteRenderer;

	private void Start()
	{
		this.spriteSheet = new Dictionary<string, Sprite>();
		this.spriteRenderer = GetComponent<SpriteRenderer>();

		this.LoadSpriteSheet();
	}

	private void LateUpdate()
	{
		if (this.LoadedSpriteSheetName != this.SpriteSheetName)
		{
			this.LoadSpriteSheet();
		}
		this.spriteRenderer.sprite = this.spriteSheet[this.spriteRenderer.sprite.name];
	}

	private void LoadSpriteSheet()
	{
		var sprites = Resources.LoadAll<Sprite>(this.SpriteSheetName);

		foreach (Sprite sp in sprites)
			this.spriteSheet.Add (sp.name, sp);

		this.LoadedSpriteSheetName = this.SpriteSheetName;
	}
}
