﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;

public class HighScore : MonoBehaviour
{

    private static List<int> highScores;
    public Text HighscoreText;

    private int MaxHighscores = 5;

    // Use this for initialization
    void Start()
    {
        HighscoreText = GetComponent<Text>();
        highScores = new List<int>();
        ReadHighScoreFromFile();
        DisplayHighScores();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void ReadHighScoreFromFile()
    {
        string path = "Assets/Resources/HighScore.txt";

        using (StreamReader reader = new StreamReader(path))
        {
            string line = reader.ReadLine();
            while (line != null)
            {
                highScores.Add(int.Parse(line));
                line = reader.ReadLine();
            }
        }
    }

    void DisplayHighScores()
    {
        highScores.Sort((x, y) => -1 * x.CompareTo(y));
        if (highScores.Count >= MaxHighscores)
            for (int i = 0; i < MaxHighscores; i++)
                HighscoreText.text += (highScores[i].ToString() + '\n');
        else
            for (int i = 0; i < highScores.Count; i++)
                HighscoreText.text += (highScores[i].ToString() + '\n');
    }

    public static void SetHighScore(int highscore)
    {
        string path = "Assets/Resources/HighScore.txt";
        using (StreamWriter writer = new StreamWriter(path, true))
        {
            writer.WriteLine(highscore);
        }
    }

}
