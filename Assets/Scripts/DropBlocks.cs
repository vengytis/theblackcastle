﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropBlocks : MonoBehaviour {

	public Transform Player;
	public float Range = 1f;
	Rigidbody2D rb;

	void Start () {
		Player = GameObject.FindGameObjectWithTag ("Player").transform;
		rb = GetComponent<Rigidbody2D> ();
	}
	
	void Update () {
		if (Player.transform.position.x >= transform.position.x - Range) {
			rb.bodyType = RigidbodyType2D.Dynamic;
			rb.mass = 100;
		}
	}
}
