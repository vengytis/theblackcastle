﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SoundMonitor : MonoBehaviour
{

    private static AudioSource audioSrc;
    public AudioClip[] BackgroundMusic = new AudioClip[6];
    private Transform Player;
    private AudioClip CurrentClip;

    private void Start()
    {
        audioSrc = GetComponent<AudioSource>();
        if (SceneManager.GetActiveScene().buildIndex != 0)
            PlayBackgroundMusic();
        else
            PlayMainMenuMusic();
    }
    private void Update()
    {
        ChangeVolume();
        if (SceneManager.GetActiveScene().buildIndex != 0)
        {
            if (Enemy_AI.isChasing)
            {
                if (CurrentClip != BackgroundMusic[1])
                    PlayMobChaseMusic();
            }
            else
            {
                if (CurrentClip != BackgroundMusic[SceneManager.GetActiveScene().buildIndex + 1])
                    PlayBackgroundMusic();
            }
        }
    }

    public void PlayBackgroundMusic()
    {
        audioSrc.Stop();
        audioSrc.loop = true;
        audioSrc.PlayOneShot(BackgroundMusic[SceneManager.GetActiveScene().buildIndex + 1]);
        CurrentClip = BackgroundMusic[SceneManager.GetActiveScene().buildIndex + 1];
		Debug.Log ("Index: " + SceneManager.GetActiveScene ().buildIndex + 1);
    }

    public void PlayMobChaseMusic()
    {
        audioSrc.Stop();
        audioSrc.loop = true;
        audioSrc.PlayOneShot(BackgroundMusic[1]);
        CurrentClip = BackgroundMusic[1];
    }

    public void PlayMainMenuMusic()
    {
        audioSrc.Stop();
        audioSrc.loop = true;
        audioSrc.PlayOneShot(BackgroundMusic[0]);
        audioSrc.volume = 0.4f;
        CurrentClip = BackgroundMusic[0];
    }

    public void ChangeVolume()
    {
        audioSrc.volume = (float)(PlayerPrefs.GetFloat("Volume")/100);
    }
}
