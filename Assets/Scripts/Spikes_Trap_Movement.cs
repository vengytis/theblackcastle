﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spikes_Trap_Movement : MonoBehaviour {

	private float StartingY;
	public float Speed;

	void Start () {
		StartingY = transform.position.y;
	}
	
	void Update () {
		
		float currentY = Mathf.PingPong(Speed * Time.time, StartingY + 1  - StartingY);
		transform.position = new Vector2(transform.position.x, StartingY + currentY);
	}
}
