﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TrapEventSystem : MonoBehaviour
{

	private Transform Player;
	public GameObject Level_3_ball;
	public GameObject Level_2_wall;
	public GameObject Level_2_sign;

	private bool Level_3_ballInstantiated;
	private static bool Level_2_jumpTriggerSet;
	private static bool Level_2_jumpTriggerUsed;

	void Start ()
	{
		Player = GameObject.FindGameObjectWithTag ("Player").transform;

		Level_3_ballInstantiated = false;
		Level_2_jumpTriggerSet = false;
	}
	
	void Update ()
	{
		AdjustTrapsByLevel ();
	}

	void AdjustTrapsByLevel ()
	{
		string currentScene = SceneManager.GetActiveScene ().name;

		switch (currentScene) {
		case "Level_2":
			Level_2 ();
			break;
		case "Level_3":
			Level_3 ();
			break;
		case "Cellar":
			Cellar ();
			break;
		}
	}

	void Level_3 ()
	{
		

		if (Player.position.x >= 22 && !Level_3_ballInstantiated) {
			Instantiate (Level_3_ball, new Vector2 (29, 7), Quaternion.identity);
			Level_3_ballInstantiated = true;
		}

	}

	void Cellar ()
	{
		Debug.Log ("Cellar");

	}

	void Level_2()
	{
		if (!Level_2_jumpTriggerUsed && Level_2_jumpTriggerSet) {
			
			Instantiate (Level_2_wall);
			Instantiate (Level_2_sign);
			Level_2_jumpTriggerUsed = true;
		}
	}

	public static void Set_Level_2_Trigger()
	{
		Level_2_jumpTriggerSet = true;
	}
}
