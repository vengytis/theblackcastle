﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{

    public float speed = 5;
    public float jump = 5;
    public float minJump = 3.5f;
    private float moveVelocity;

    private bool isGrounded = false;
    private Animator anim;
    private Rigidbody2D rb;
    private EdgeCollider2D ecGroundCheck;
    public GameObject Projectile;
    public GameObject MainMenuPanel;
    public GameObject AndroidControls;
    private GameObject Android_Left;
    private GameObject Android_Right;
    private GameObject Android_Up;
    private GameObject Android_Down;

    private bool gamePaused;

    private void Start()
    {
        gamePaused = false;
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        ecGroundCheck = GetComponent<EdgeCollider2D>();
        AndroidControls = GameObject.FindGameObjectWithTag("AndroidControls");
        
    }
    void Update()
    {
        Controller();
   //     Debug.Log(rb.velocity.y);
    }

    void OnCollisionStay2D(Collision2D col)
    {
        if (col.gameObject.layer == LayerMask.NameToLayer("Ground"))
            isGrounded = true;
    }

    private void OnCollisionExit2D(Collision2D col)
    {
        if (col.gameObject.layer == LayerMask.NameToLayer("Ground"))
            isGrounded = false;
    }

    void Attack()
    {
        anim.SetTrigger("Attack");
    }

    void Controller()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (!MainMenuPanel.activeSelf)
            {
                MainMenuPanel.SetActive(true);
                gamePaused = true;
                Time.timeScale = 0;
            }
            else
            {
                gamePaused = false;
                MainMenuPanel.SetActive(false);
                Time.timeScale = 1;
            }

        }

        if (!gamePaused)
        {
            if (Input.GetKey(KeyCode.Space))
            {
                Attack();
            }
            else
            {
                anim.ResetTrigger("Attack");
            }

            //if (rb.velocity.y > 0 && !Input.GetKey(KeyCode.W))
            if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow))
            {
                Jump();
            }

            moveVelocity = rb.velocity.x;

            if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
            {
                MoveLeft();
            }
            else if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
            {
                MoveRight();
            }
            else
            {
                moveVelocity = 0;
                anim.SetBool("isWalking", false);
            }
            rb.velocity = new Vector2(moveVelocity, rb.velocity.y);
        }
    }

    void ShootProjectile()
    {
        if (transform.localScale.x > 0)
            Instantiate(Projectile, new Vector2(transform.position.x + 0.5f, transform.position.y), Quaternion.identity);
        else
            Instantiate(Projectile, new Vector2(transform.position.x - 0.5f, transform.position.y), Quaternion.identity);
    }

    public void MoveLeft()
    {
        anim.SetBool("isWalking", true);
        moveVelocity = -speed;
        transform.localScale = new Vector2(-1, 1);
    }

    public void MoveRight()
    {
        anim.SetBool("isWalking", true);
        moveVelocity = speed;
        transform.localScale = new Vector2(1, 1);
    }

    public void Jump()
    {
        if (isGrounded)
        {
            rb.velocity = new Vector2(rb.velocity.x, jump);
            //	anim.Play ("Jump");
        }
    }

    public void VelocityBiggerThanZero()
    {
        if (rb.velocity.y > 0)
            anim.Play("Jump");
    }
}
