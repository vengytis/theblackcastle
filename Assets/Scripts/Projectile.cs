﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

    private Rigidbody2D rb;
    private Transform Player;
    private Vector2 Direction;
    private float Range = 5f;
    private float Speed = 5f;
    private float StartingX;
    private float Distance;

	void Start () {
        Distance = 0;
        rb = GetComponent<Rigidbody2D>();
        StartingX = transform.position.x;
        Player = GameObject.FindGameObjectWithTag("Player").transform;
        if (Player.transform.localScale.x > 0)
        {
            Direction = Vector2.right;
        }
        else
        {
            transform.localScale = new Vector2(-1, 1);
            Direction = Vector2.left;
        }
	}
	
	void Update () {
        OutOfRange();
        rb.velocity = Direction * Speed;
	}

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            //Debug.Log ("Collides");
            //  Enemy_AI.ReceiveDamage(collision.transform.position.x, transform.position.x);
            Rigidbody2D enemyRB = collision.gameObject.GetComponent<Rigidbody2D>();
            /*if (enemyRB.position.x > transform.position.x)
                enemyRB.AddForce(new Vector2(enemyRB.position.x + 20, enemyRB.position.y + 1));
            // Destroy(collision.gameObject);*/
            collision.transform.SendMessage("LoseHp", 5, SendMessageOptions.DontRequireReceiver);
            Destroy(gameObject);
        }
    }

    void OutOfRange()
    {
        Distance += Speed * Time.deltaTime;

        if (Distance >= Range)
            Destroy(gameObject);
    } 
}
