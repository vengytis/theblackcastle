﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneMonitor : MonoBehaviour
{
    private static int Score = 0;

    private static float TimeElapsedSeconds = 0;
 //   private GameObject[] Enemies;
    public Transform Player;

    private void Start()
    {
   //     Enemies = GameObject.FindGameObjectsWithTag("Enemy");
      //  Debug.Log("Enemies in scene: " + Enemies.Length);
        Player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    private void Update()
    {
        TimeElapsedSeconds += Time.deltaTime;
    }

    public static void AddScore(int score)
    {
        Score += score;
    }

    public static int GetScore()
    {
        return Score;
    }

    public static int GetTimeElapsed()
    {
        return (int)(TimeElapsedSeconds % 60);
    }

    public static void SetScore(int score)
    {
        Score = score;
    }

}