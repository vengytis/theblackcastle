﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class ScoreSetter : MonoBehaviour {

    private Text Score;

	void Start () {
        Score = GetComponent<Text>();
        Score.text = "Score: 0";
	}
	
	void Update () {
        Score.text = "Score: " + SceneMonitor.GetScore();
	}
}
