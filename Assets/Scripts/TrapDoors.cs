﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapDoors : MonoBehaviour {

	public float Range = 5f;
	public Transform Player;
	public GameObject EnemyPrefab;
	private bool opened;

	void Start () {
		opened = false;
		Player = GameObject.FindGameObjectWithTag ("Player").transform;
	}
	
	void Update () {
		float distance = Vector2.Distance (transform.position, Player.transform.position);
		if (distance < Range && !opened) {
			opened = true;
			SpriteRenderer rend = GetComponent<SpriteRenderer>();
			rend.enabled = false;
			GameObject enemy = Instantiate (EnemyPrefab, new Vector2(transform.position.x, transform.position.y - 0.4f), Quaternion.identity);
			enemy.GetComponent<Enemy_AI> ().isPatrol = false;
		}
	}
}
